﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

using MaGE.Geometry;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;

namespace MaGE.Controls
{
    public class DummyMapControl : Image
    {
        private SegmentList list;
        Random rand = new Random();

        public DummyMapControl(SegmentList list)
        {
            this.list = list;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext dc)
        {
            base.OnRender(dc);
            
            
            for (int i = 0; i < 5000; ++i)
            {
                double kx = rand.Next(1000) - 500;
                double ky = rand.Next(1000) - 500;
                Pen pen = new Pen(new SolidColorBrush(Color.FromArgb((byte)rand.Next(255), (byte)rand.Next(255), (byte)rand.Next(255), (byte)rand.Next(255))), 1);
                foreach (Segment s in list.GetSegments())
                {
                    dc.DrawLine(pen, new Point(s.StartPoint.X + kx, s.StartPoint.Y + ky), new Point(s.EndPoint.X + kx, s.EndPoint.Y + ky));
                }
            }
        }


    }
}
