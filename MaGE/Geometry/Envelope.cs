﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public class Envelope : Geometry
    {
        #region Fields
        private double minX;
        private double minY;
        private double maxX;
        private double maxY;
        #endregion Fields

        #region Properties
        public double Height
        {
            get { return maxY - minY; }
        }

        public double Width
        {
            get { return maxY - minY; }
        }

        public double XMax
        {
            get { return maxX; }
        }

        public double XMin
        {
            get { return minX; }
        }

        public double YMax
        {
            get { return maxY; }
        }

        public double YMin
        {
            get { return minY; }
        }

        public override Envelope Extent
        {
            get 
            {
                if (IsEmpty)
                    return null;
                return this; 
            }
        }

        public override GeometryType GeometryType
        {
            get { return GeometryType.Envelope; }
        }

        public override bool IsEqual(Geometry other)
        {
            if (spatialReference == other.SpatialReference)
            {
                var envelope = other as Envelope;
                if ((envelope != null) && (envelope.XMin == XMin) && (envelope.XMax == XMax) && (envelope.YMin == YMin) && (envelope.YMax == YMax))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion Properties

        #region Constructors
        public Envelope(MapPoint a, MapPoint b)
        {
            if ((a != null) && (b != null)) {
                properties = properties ^ GeometryProperties.IsEmpty;
                this.minX = Math.Min(a.X, b.X);
                this.maxX = Math.Max(a.X, b.X);
                this.minY = Math.Min(a.Y, b.Y);
                this.maxY = Math.Max(a.Y, b.Y);
                base.spatialReference = a.SpatialReference;
            }
        }

        public Envelope(double x1, double y1, double x2, double y2)
        {
            this.minX = Math.Min(x1, x2);
            this.maxX = Math.Max(x1, x2);
            this.minY = Math.Min(y1, y2);
            this.maxY = Math.Max(y1, y2);
            properties = properties ^ GeometryProperties.IsEmpty;
        }

        public Envelope(double x1, double y1, double x2, double y2, SpatialReference spatialReference)
            : this(x1, y1, x2, y2)
        {
            base.spatialReference = spatialReference;
        }
        #endregion Constructors

        #region Methods
        public Envelope Expand(double factor)
        {
            if (factor < 0)
                return null;

            double dx = Width / 2.0d;
            double dy = Height / 2.0d;
            dx = dx * factor - dx;
            dy = dy * factor - dy;
            return new Envelope(minX - dx, minY - dy, maxX + dx, maxY + dy);
        }

        public MapPoint GetCenter()
        {
            return new MapPoint((XMax + XMin) / 2.0d, (YMax + YMin) / 2.0d, SpatialReference);
        }

        /// <summary>
        /// Calculates the intersection between this instance and the specified envelope.
        /// </summary>
        /// <param name="other">Envelope to intersect with</param>
        /// <returns>The intersecting envelope or null if they don't intersect.</returns>
        public Envelope Intersection(Envelope other)
        {
            if (Intersects(other))
            {
                return new Envelope(Math.Max(XMin, other.XMin), Math.Max(YMin, other.YMin), Math.Min(XMax, other.XMax), Math.Min(YMax, other.YMax), SpatialReference);
            }
            return null;
        }

        /// <summary>
        /// Returns true if this instance intersects an envelope.
        /// </summary>
        /// <param name="other">Envelope to test against</param>
        /// <returns>True if they intersect</returns>
        public bool Intersects(Envelope other)
        {
            if ((other.SpatialReference == SpatialReference) 
                && (XMax >= other.XMin) 
                && (XMin <= other.XMax) 
                && (YMax >= other.YMin) 
                && (YMin <= other.YMax))
                return true;
            return false;
        }

        public override string ToString()
        {
            MapPoint upLeft = new MapPoint(XMin, YMax);
            MapPoint downRight = new MapPoint(XMax, YMin);
            return "{ ul:" + upLeft.ToString() + ", dr:" + downRight.ToString() + "}";
        }

        public Envelope Union(Envelope other)
        {
            return new Envelope(Math.Max(XMax, other.XMax), Math.Max(YMax, other.YMax), Math.Min(XMin, other.XMin), Math.Min(YMin, other.YMin));
        }

        public Envelope Union(MapPoint point)
        {
            return new Envelope(Math.Max(XMax, point.X), Math.Max(YMax, point.Y), Math.Min(XMin, point.X), Math.Min(YMin, point.Y));
        }

        #endregion Methods

        #region Operators
        public static Envelope operator &(Envelope a, Envelope b)
        {
            return a.Intersection(b);
        }

        public static Envelope operator +(Envelope a, Envelope b)
        {
            return a.Union(b);
        }

        public static Envelope operator *(Envelope envelope, double multipiler)
        {
            return envelope.Expand(multipiler);
        }

        public static Envelope operator +(Envelope a, MapPoint b)
        {
            return a.Union(b);
        }

        #endregion Operators

    }
}
