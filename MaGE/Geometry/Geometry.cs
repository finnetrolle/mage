﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public enum GeometryType
    {
        Unknown,
        Point,
        Multipoint,     // Point Container
        Polyline,
        Polygon,
        Envelope,       // Just a rectangle
        Multiline,      // Polyline container
        Multipolygon    // Polygon container
    }

    [Flags]
    public enum GeometryProperties
    {
        None = 0x0,
        IsEmpty = 0x1,
        HasZ = 0x2,
        HasM = 0x4
    }

    public abstract class Geometry
    {

        #region Fields
        protected SpatialReference spatialReference;
        protected bool isNullOrEmpty = true;
        protected GeometryProperties properties = GeometryProperties.IsEmpty;

        #endregion Fields

        #region Properties
        public bool HasZ { get { return this.properties == GeometryProperties.HasZ; } }
        public bool HasM { get { return this.properties == GeometryProperties.HasM; } }
        public bool IsEmpty { get { return this.properties == GeometryProperties.IsEmpty; } }
        public abstract Envelope Extent { get; }
        public SpatialReference SpatialReference { get {return spatialReference;} }
        public virtual int Dimension { get { return 0;} }
        public abstract GeometryType GeometryType { get; }

        #endregion Properties

        #region Methods
        public abstract bool IsEqual(Geometry other);

        public static bool IsNullOrEmpty(Geometry geometry)
        {
            return geometry.IsEmpty;
        }

        #endregion Methods

    }
}
