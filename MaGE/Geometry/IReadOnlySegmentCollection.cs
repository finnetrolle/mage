﻿using System;
namespace MaGE.Geometry
{
    public interface IReadOnlySegmentCollection
    {
        MapPoint FirstPoint { get; }
        MapPoint GetPoint(int pointIndex);
        System.Collections.Generic.IEnumerable<MapPoint> GetPoints();
        Segment GetSegment(int segmentIndex);
        System.Collections.Generic.IEnumerable<Segment> GetSegmentsContainsPoint(MapPoint point);
        bool IsClosed { get; }
        MapPoint LastPoint { get; }
        int PointCount { get; }
        Envelope ProcessEnvelope();
        int SegmentCount { get; }
        bool IsEqual(IReadOnlySegmentCollection other);
    }
}
