﻿using System;
namespace MaGE.Geometry
{
    public interface ISegmentCollection : IReadOnlySegmentCollection
    {
        void AddPoint(MapPoint point);
        void AddPoints(System.Collections.Generic.IEnumerable<MapPoint> points);
        void Close();
        void RemovePoint(int pointIndex);
        void Unclose();
    }
}
