﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    class LineSegment : Segment
    {

        #region Constructors
        public LineSegment(MapPoint start, MapPoint end)
        {
            base.startPoint = start;
            base.endPoint = end;
            base.spatialReference = start.SpatialReference;
        }

        public LineSegment(double startX, double startY, double endX, double endY) 
        {
            base.startPoint = new MapPoint(startX, startY);
            base.endPoint = new MapPoint(endX, endY);
        }

        public LineSegment(double startX, double startY, double endX, double endY, SpatialReference spatialReference)
        {
            base.startPoint = new MapPoint(startX, startY, spatialReference);
            base.endPoint = new MapPoint(endX, endY, spatialReference);
            base.spatialReference = spatialReference;
        }

        #endregion Constructors

        #region Methods
        /// <summary>
        /// Method returns true if current equals to other. Direction is important!
        /// </summary>
        /// <param name="other">Other segment</param>
        /// <returns>True if segment is fully equal</returns>
        public override bool IsEqual(Segment other)
        {
            if ((other.SpatialReference == spatialReference)
                && (startPoint.IsEqual(other.StartPoint))
                && (endPoint.IsEqual(other.EndPoint)))
                return true;
            return false;
        }

        public override string ToString()
        {
            return "{start:" + StartPoint + ", end:" + EndPoint + ", closed:" + IsClosed + ", type: LineSegment}";
        }

        public virtual void Some()
        {

        }

        #endregion Methods
        
    }
}
