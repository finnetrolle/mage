﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public class MapPoint : Geometry
    {
        #region Private fields
        private double x;
        private double y;
        private double z;
        private double m;
        

        #endregion private fields

        #region Properties
        public double X { get {return x;} }
        public double Y { get {return y;} }
        public double Z { get {return z;} }
        public double M { get {return m;} }

        public override Envelope Extent
        {
            get { return new Envelope(this, this); }
        }

        public override GeometryType GeometryType
        {
            get { return GeometryType.Point; }
        }

        public override int Dimension
        {
            get { return 0; }
        }

        #endregion Properties

        #region Operators
        public static MapPoint operator +(MapPoint a, MapPoint b)
        {
            if (a.SpatialReference != b.SpatialReference)
                throw new Exception("MapPoints have different Spatial References");
            if (a.HasZ || b.HasZ)
            {
                if (a.HasM || b.HasM)
                {
                    return new MapPoint(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.M + b.M, a.SpatialReference);
                }
                return new MapPoint(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.SpatialReference);
            }
            return new MapPoint(a.X + b.X, a.Y + b.Y, a.SpatialReference);
        }

        public static MapPoint operator -(MapPoint a, MapPoint b)
        {
            if (a.SpatialReference != b.SpatialReference)
                throw new Exception("MapPoints have different Spatial References");
            if (a.HasZ || b.HasZ)
            {
                if (a.HasM || b.HasM)
                {
                    return new MapPoint(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.M - b.M, a.SpatialReference);
                }
                return new MapPoint(a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.SpatialReference);
            }
            return new MapPoint(a.X - b.X, a.Y - b.Y, a.SpatialReference);
        }
        #endregion Operators

        #region Constructors
        public MapPoint(double x, double y)
        {
            this.x = x;
            this.y = y;
            properties = properties ^ GeometryProperties.IsEmpty;
        }

        public MapPoint(double x, double y, SpatialReference spatialReference) 
            : this(x, y)
        {
            base.spatialReference = spatialReference;
        }

        public MapPoint(double x, double y, double z)
            : this(x, y)
        {
            this.z = z;
            properties = properties | GeometryProperties.HasZ;
        }

        public MapPoint(double x, double y, double z, SpatialReference spatialReference)
            : this(x, y, z)
        {
            base.spatialReference = spatialReference;
        }

        public MapPoint(double x, double y, double z, double m)
            : this(x, y, z)
        {
            this.m = m;
            properties = properties | GeometryProperties.HasM;
        }

        public MapPoint(double x, double y, double z, double m, SpatialReference spatialReference)
            : this(x, y, z, m)
        {
            base.spatialReference = spatialReference;
        }

        #endregion Constructors

        #region Methods
        public MapPoint MoveTo(double x, double y)
        {
            this.x = x;
            this.y = y;
            return this;
        }

        public override bool IsEqual(Geometry other)
        {
            if (other.SpatialReference == base.spatialReference)
            {
                var point = other as MapPoint;
                if ((point != null) && (point.X == x) && (point.Y == y) && (point.Z == z) && (point.M == m))
                {
                    return true;
                }
            }
            return false;
        }

        public override string ToString()
        {
            return "{ x: " + x + ", y: " + y + "}";
        }
        #endregion Methods

    }
}
