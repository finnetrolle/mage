﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public class Polyline : Geometry
    {

        #region Fields
        private SegmentList segments;
        private Envelope envelope = null;
        

        #endregion Fields

        #region Properties
        public double LineWidth { get; set; }

        public override Envelope Extent
        {
            get { return envelope; }
        }

        public override GeometryType GeometryType
        {
            get { return GeometryType.Polyline; }
        }

        public IReadOnlySegmentCollection SegmentList
        {
            get { return segments; }
        }

        #endregion Properties

        #region Constructors
        public Polyline()
        {
            segments = new SegmentList();
        }

        public Polyline(SegmentList segments)
        {
            this.segments = segments;
            RecalcEnvelope();
        }

        #endregion Constructors

        #region Methods
        public override bool IsEqual(Geometry other)
        {
            if (other.GeometryType == GeometryType)
            {
                var line = other as Polyline;
                if ((line != null) && (line.SegmentList.IsEqual(SegmentList)))
                    return true;
                // TODO add some tests for other fields
            }
            return false;
        }

        public Envelope AddPoint(MapPoint point)
        {
            segments.AddPoint(point);
            return RecalcEnvelope();
        }

        public Envelope AddPoints(IEnumerable<MapPoint> points)
        {
            foreach (MapPoint p in points)
            {
                segments.AddPoint(p);
            }
            return RecalcEnvelope();
        }

        public Envelope RemovePoint(int pointIndex)
        {
            segments.RemovePoint(pointIndex);
            return RecalcEnvelope();
        }

        private Envelope RecalcEnvelope()
        {
            this.envelope = segments.ProcessEnvelope();
            return this.envelope;
        }

        #endregion Methods

        #region Operators
        #endregion Operators


        
    }
}
