﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public abstract class Segment
    {
        #region Fields
        protected SpatialReference spatialReference;
        protected MapPoint startPoint;
        protected MapPoint endPoint;

        #endregion Fields

        #region Properties
        public MapPoint StartPoint { get { return startPoint; } }
        public MapPoint EndPoint { get { return endPoint; } }
        public bool IsClosed 
        {
            get 
            {
                if (startPoint.IsEqual(endPoint))
                    return true;
                return false;
            } 
        }
        public SpatialReference SpatialReference { get { return spatialReference; } }

        #endregion Properties

        #region Constructors
        protected Segment()
        {
                
        }

        #endregion Constructors

        #region Methods
        public abstract bool IsEqual(Segment other);


        #endregion Methods

        #region Operators
        //public static Segment operator +(Segment a, Segment b);
        //public static Segment operator -(Segment a, Segment b);

        #endregion Operators
    }
}
