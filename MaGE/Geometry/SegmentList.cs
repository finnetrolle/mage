﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGE.Geometry
{
    public class SegmentList : MaGE.Geometry.ISegmentCollection, IReadOnlySegmentCollection
    {
        #region Fields
        protected List<Segment> segments;
        protected MapPoint bufferedPoint = null;

        #endregion Fields

        #region Properties
        
        public int SegmentCount
        {
            get { return segments.Count; }
        }
        
        /// <summary>
        /// PointCount = SegmentCount + 1 even for closed polylines, because last point (equal to first point) defines Closed property
        /// </summary>
        public int PointCount
        {
            get 
            {
                if (bufferedPoint != null)
                    return 1;
                return ((segments.Count > 0) ? segments.Count + 1 : 0);
            }
        }
        
        /// <summary>
        /// Returns first point of point array. If there are no segments, returns bufferedPoint
        /// </summary>
        public MapPoint FirstPoint
        {
            get 
            {
                if (segments.Count > 0)
                    return segments[0].StartPoint;
                return bufferedPoint;
            }
        }
        
        /// <summary>
        /// Returns last point of point array. If there are no segments, returns bufferedPoint
        /// </summary>
        public MapPoint LastPoint
        {
            get
            {
                if (segments.Count > 0)
                    return segments[segments.Count - 1].EndPoint;
                return bufferedPoint;
            }
        }

        /// <summary>
        /// Check closed segment
        /// </summary>
        public bool IsClosed
        {
            get
            {
                if (LastPoint.IsEqual(FirstPoint))
                    return true;
                return false;
            }
        }
        
        #endregion Properties

        #region Constructors
        public SegmentList()
        {
            segments = new List<Segment>();
        }

        public SegmentList(IEnumerable<MapPoint> points) 
            : this()
        {
            AddPoints(points);
        }

        #endregion Constructors

        #region Methods
        /// <summary>
        /// Create and add segment with start = LastPoint, end = point
        /// </summary>
        /// <param name="point">point to add</param>
        public void AddPoint(MapPoint point)
        {
            if (segments.Count == 0)
            {
                if (bufferedPoint == null)
                {
                    bufferedPoint = point;
                }
                else
                {
                    segments.Add(new LineSegment(bufferedPoint, point));
                    bufferedPoint = null;
                }
            }
            else
            {
                segments.Add(new LineSegment(LastPoint, point));
            }
        }

        /// <summary>
        /// Add collection of points, creating segments
        /// </summary>
        /// <param name="points">collection of points</param>
        public void AddPoints(IEnumerable<MapPoint> points)
        {
            foreach (MapPoint point in points)
            {
                AddPoint(point);
            }
        }

        /// <summary>
        /// Returns point by index
        /// </summary>
        /// <param name="pointIndex">index of point</param>
        /// <returns>Point if exists, null otherwise</returns>
        public MapPoint GetPoint(int pointIndex)
        {
            if ((PointCount > pointIndex) && (pointIndex >= 0))
            {
                if ((pointIndex == 0) && (bufferedPoint != null))
                    return bufferedPoint;
                if (pointIndex == SegmentCount)
                    return LastPoint;
                return segments[pointIndex].StartPoint;
            }
            return null;
        }

        public Segment GetSegment(int segmentIndex)
        {
            if ((segmentIndex >= 0) && (segmentIndex < SegmentCount))
            {
                return segments[segmentIndex];
            }
            return null;
        }

        public IEnumerable<Segment> GetSegmentsContainsPoint(MapPoint point)
        {
            List<Segment> result = new List<Segment>();
            foreach (Segment s in segments)
            {
                if ((point.IsEqual(s.StartPoint)) || (point.IsEqual(s.EndPoint)))
                    result.Add(s);
            }
            return result;
        }

        /// <summary>
        /// Returns all points (last can be == first)
        /// </summary>
        /// <returns>Points!</returns>
        public IEnumerable<MapPoint> GetPoints()
        {
            List<MapPoint> points = new List<MapPoint>();
            if (bufferedPoint != null)
            {
                points.Add(bufferedPoint);
            }
            else
            {
                foreach (Segment s in segments)
                {
                    points.Add(s.StartPoint);
                }
                points.Add(LastPoint);
            }
            return points;
        }

        public void RemovePoint(int pointIndex)
        {
            if (pointIndex < PointCount)
            {
                if (bufferedPoint != null) {
                    bufferedPoint = null;
                    return;
                }
                if (pointIndex == 0)
                {
                    segments.RemoveAt(0);
                    return;
                }
                if (pointIndex == PointCount - 1)
                {
                    segments.RemoveAt(SegmentCount - 1);
                    return;
                }
                // close previous segment
                segments[pointIndex - 1] = new LineSegment(segments[pointIndex - 1].StartPoint, segments[pointIndex].EndPoint);
                // remove current segment
                segments.RemoveAt(pointIndex);
            }
        }

        /// <summary>
        /// Method add segment to connect FirstPoint and LastPoint if they are not equal
        /// </summary>
        public void Close()
        {
            if (!IsClosed)
            {
                segments.Add(new LineSegment(LastPoint, FirstPoint));
            }
        }

        /// <summary>
        /// Method removes last segment if segment list is closed
        /// </summary>
        public void Unclose()
        {
            if (IsClosed)
            {
                segments.RemoveAt(SegmentCount - 1);
            }
        }

        public Envelope ProcessEnvelope()
        {
            if (PointCount > 0)
            {
                Envelope result = new Envelope(FirstPoint, LastPoint);
                for (int i = 0; i < segments.Count; ++i)
                {
                    result.Union(segments[i].EndPoint);
                }
                return result;
            }
            return null;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{ closed:");
            sb.Append(IsClosed);
            sb.Append(", segments: ");
            int i = 0;
            for (; i < SegmentCount-1; ++i) {
                sb.Append(segments[i].ToString());
                sb.Append(", ");
            }
            if (i < SegmentCount) {
                sb.Append(segments[i].ToString());
            }
            sb.Append("}");
            return sb.ToString();
        }

        public bool IsEqual(IReadOnlySegmentCollection other)
        {
            if ((other.SegmentCount == SegmentCount)
                && (other.PointCount == PointCount))
            {
                for (int i = 0; i < SegmentCount; ++i)
                {
                    if (GetSegment(i).IsEqual(other.GetSegment(i)) == false)
                        return false;
                }
                return true;
            }
            return false;
        }

        public IEnumerable<Segment> GetSegments()
        {
            return this.segments;
        }

        #endregion Methods

    }
}
