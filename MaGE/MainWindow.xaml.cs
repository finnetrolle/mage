﻿using MaGE.Geometry;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Threading;

using MaGE.Controls;

namespace MaGE
{
    /*
     
        #region Fields
        #endregion Fields

        #region Properties
        #endregion Properties

        #region Constructors
        #endregion Constructors

        #region Methods
        #endregion Methods

        #region Operators
        #endregion Operators
      
     */

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DummyMapControl dmc;

        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("hello");



            SegmentList line = new SegmentList();
            line.AddPoint(new MapPoint(0, 0));
            line.AddPoint(new MapPoint(200, 300));
            line.AddPoint(new MapPoint(300, 200));
            Console.WriteLine(line);
            //line.Close();
            Console.WriteLine(line);



            dmc = new DummyMapControl(line);
            this.mainGrid.Children.Add(dmc);
            //this.MapBox.Source = bmp;
        }
    }
}
