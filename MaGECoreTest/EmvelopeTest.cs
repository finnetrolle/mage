﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaGE.Geometry;
using FluentAssertions;
using FluentAssertions.Primitives;

namespace MaGECoreTest
{
    [TestClass]
    public class EnvelopeTest : IDisposable
    {
        MapPoint A = new MapPoint(2, 5);
        MapPoint B = new MapPoint(4, 1);
        MapPoint C = new MapPoint(1, 4);
        MapPoint D = new MapPoint(5, 2);
        MapPoint E = new MapPoint(3, 3);
        MapPoint F = new MapPoint(6, 0);
        MapPoint M = new MapPoint(5, 6);
        MapPoint N = new MapPoint(6, 2);

        Envelope AB;
        Envelope CD;
        Envelope EF;
        Envelope MN;

        public EnvelopeTest()
        {
            AB = new Envelope(A, B);
            CD = new Envelope(C, D);
            EF = new Envelope(E, F);
            MN = new Envelope(M, N);
        }
        
        [TestMethod, TestCategory("Testing Envelope.Intersects()")]
        public void TestIntersects()
        {
            AB.Intersects(CD).Should().Be(true);
            AB.Intersects(EF).Should().Be(true);
            AB.Intersects(MN).Should().Be(false);
            CD.Intersects(MN).Should().Be(true);
            MN.Intersects(EF).Should().Be(true);
        }

        [TestMethod, TestCategory("Testing Envelope.Intersection()")]
        public void TestIntersection()
        {
            (CD & EF).Should().BeEqual(new Envelope(E, D));
            (CD & EF).Should().NotBeEqual(new Envelope(5, 3, 6, 2));
            (MN & EF).Should().BeEqual(new Envelope(5, 3, 6, 2));
        }

        [TestMethod]
        public void TestIsEqual()
        {
            new Envelope(A, F).Should().BeEqual(new Envelope(2, 5, 6, 0));
            new Envelope(E, D).Should().BeEqual(new Envelope(5, 2, 3, 3));
            new Envelope(C, N).Should().BeEqual(new Envelope(6, 2, 1, 4));
            new Envelope(1, 1, 2, 2).Should().NotBeEqual(new Envelope(1, 2, 3, 4));
        }

        [TestMethod]
        public void TestUnion()
        {
            new Envelope(3, 6, 6, 0).Should().BeEqual(MN + EF);
            new Envelope(new MapPoint(1, 6), N).Should().BeEqual(CD + MN);
            new Envelope(A, B).Should().NotBeEqual(CD + EF);
        }

        [TestMethod]
        public void TestUnionWithDot()
        {
            new Envelope(0, 0, 2, 2).Should().BeEqual((new Envelope(0, 0, 1, 1) + new MapPoint(2, 2)));
        }

        [TestMethod]
        public void TestExpand()
        {
            Envelope Ri = new Envelope(0, 0, 10, 10);
            Envelope Rl = new Envelope(-1, -1, 11, 11); // enLarged
            Envelope Rs = new Envelope(1, 1, 9, 9); // Shrinked

            Envelope r = Ri * 1.2f;

            (Ri * 1.2).Should().BeEqual(Rl);
            (Ri * 0.8).Should().BeEqual(Rs);
        }

        [TestMethod]
        public void TestGetCenter()
        {
            new Envelope(-100, -100, 100, 100).GetCenter().IsEqual(new MapPoint(0, 0)).Should().Be(true);
            new Envelope(-50, 20, -110, -20).GetCenter().IsEqual(new MapPoint(-80, 0)).Should().Be(true);
        }

        public void Dispose()
        {
            
        }
    }

    public static class EnvelopeExtension
    {
        public static void BeEqual(this ObjectAssertions assertions, Envelope instance)
        {
            Envelope subj = assertions.Subject as Envelope;
            Assert.IsTrue(subj.IsEqual(instance));
        }

        public static void NotBeEqual(this ObjectAssertions assertions, Envelope instance)
        {
            Envelope subj = assertions.Subject as Envelope;
            Assert.IsFalse(subj.IsEqual(instance));
        }
    }
}
