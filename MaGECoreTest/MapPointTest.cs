﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MaGE.Geometry;
using FluentAssertions;
using FluentAssertions.Primitives;

namespace MaGECoreTest
{
    [TestClass]
    public class MapPointTest
    {
        MapPoint A = new MapPoint(0, 0);
        MapPoint B = new MapPoint(1, 1);
        MapPoint C = new MapPoint(5, 5);
        MapPoint D = new MapPoint(10, 10);
        MapPoint E = new MapPoint(1, 0);
        MapPoint F = new MapPoint(0, 1);
        MapPoint G = new MapPoint(4, 4);

        [TestMethod]
        public void TestMapPointOperators()
        {
            (A + B).Should().BeEqual(B);
            D.Should().BeEqual(C + E + F + B + B + B + B);
            new MapPoint(6, 6).Should().BeEqual(B + C);
            (E + C).Should().NotBeEqual(new MapPoint(5, 6));
            (D - C).Should().NotBeEqual(E);
            G.Should().BeEqual(C - B);
        }

        [TestMethod]
        public void TestIsEqual()
        {
            MapPoint A = new MapPoint(1, 2, 3, 4);
            MapPoint B = new MapPoint(4, 3, 2, 1);
            MapPoint C = new MapPoint(1, 2, 3, 4);
            MapPoint D = A;

            A.Should().NotBeEqual(B);
            A.Should().BeEqual(C);
            A.Should().BeEqual(D);
            D.Should().BeEqual(C);
            B.Should().NotBeEqual(D);
            new MapPoint(15, 25).Should().BeEqual(new MapPoint(15, 25));
        }

        [TestMethod]
        public void TestProperties()
        {
            A.X.Should().Be(0);
            A.Y.Should().Be(0);
            D.X.Should().Be(10);
        }

        [TestMethod]
        public void TestMoveTo()
        {
            MapPoint result = A.MoveTo(500, 600);
            result.X.Should().Be(500);
            result.Y.Should().Be(600);
        }
    }

    public static class MapPointExtension
    {
        public static void BeEqual(this ObjectAssertions assertions, MapPoint instance)
        {
            MapPoint subj = assertions.Subject as MapPoint;
            Assert.IsTrue(subj.IsEqual(instance));
        }

        public static void NotBeEqual(this ObjectAssertions assertions, MapPoint instance)
        {
            MapPoint subj = assertions.Subject as MapPoint;
            Assert.IsFalse(subj.IsEqual(instance));
        }
    }
}
